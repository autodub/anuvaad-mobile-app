## Anuvaad mobile application to translate/dub videos,speech,text

In India there are many students/aspirants and enthusiastic people who want to learn new things. Language barrier should not be an obstacle for them. In this current internet world high quality educational videos are created in English. But we could not benefit from this because in our country more than 70% people could not understand and comprehend in English.
There are many text translation, speech translation applications available online but that partly solves our problem. So we have come up with a solution, 'Anuvaad' a mobile application which translates/dubs video and audio content helping  our fellow people to watch videos in their desired language. 

## Want to use our app.
Download anuvaad.apk and install it in your application. Within short time we will make it available on play store. 

## Feature and Uses of Anuvaad

1. Translate/dub educational or e-learning videos , motivational, informational or spiritual speeches into vernacular languages.
 -Upload a video or share video link which you want to translate. Within a few minutes the video will be translated into your desired language.
2. Speech-to-Speech and Text-to-Text translation.
 -Record speech or conversation and in real time translated speech is generated.
3. Translate text inside documents,nameplates, signboards etc by simply capturing an image.
 -Capture images. Text inside images is extracted and translated in real time.


## Things Done so far 
We are in private Beta now. Deployed our application in various smart phones, fixing bugs and issues.

1. Deployed Speech to Speech translation into our mobile application
2. Deployed Text to Text translation into our mobile application
3. Developed video translation pipeline and deployed it on the cloud.
4. Developed REST api for video translation.

## TODO

1. Integrate video translation into our mobile application.
2. Fix bugs and issues.
3. Launch our application.


## Want to contribute or any queries

Mail me: harsha.autodub@gmail.com