package com.example.myapplication;

import java.util.ArrayList;

public class GlobalVars {
    public static String BASE_REQ_URL = "https://translate.yandex.net/api/v1.5/tr.json/";
    public static ArrayList<String> LANGUAGE_CODES = new ArrayList<String>();
    public static ArrayList<String> GOOGLE_LANGUAGE_CODES = new ArrayList<String>();
    public static int DEFAULT_LANG_POS = 4;
    public static int DEFAULT_FROM_LANG_POS = 2;
    public GlobalVars(){}
}
