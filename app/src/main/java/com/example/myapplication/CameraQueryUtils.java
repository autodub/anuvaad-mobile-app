package com.example.myapplication;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.namespace.QName;

import static com.example.myapplication.GlobalVars.LANGUAGE_CODES;

public class CameraQueryUtils {
    public static final String LOG_TAG = QueryUtils.class.getName();

    private CameraQueryUtils() {
    }
    //  METHOD TO CREATE URL OBJECT FROM STRING ARGUMENT
    private static URL createUrl(String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Problem building the URL ", e);
        }

        return url;
    }

    //  METHOD TO MAKE HTTP REQUEST AND FETCH JSON OUTPUT
    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";
        if (url == null) {
            return jsonResponse;
        }
        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            urlConnection.setRequestMethod("POST");
            urlConnection.connect();
            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Error response code: " + urlConnection.getResponseCode());
                Log.e(LOG_TAG, url.toString());
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem retrieving the JSON results.", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    private static String readFromStream(InputStream inputStream) throws IOException{
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }
    //  METHOD TO EXTRACT TRANSLATION FROM JSON RESPONSE
    private static String extractFromJsonTranslation(String stringJSON) {
        String translation = "";
        if (TextUtils.isEmpty(stringJSON)) {
            return null;
        }
        // Try to parse the JSON response string.
        try {
            // Create a JSONObject from the JSON response string
            JSONObject baseJsonResponse = new JSONObject(stringJSON);
            JSONObject stringArray = baseJsonResponse.getJSONObject("data");
            JSONArray resultTranslations = stringArray.getJSONArray("translations");
            JSONObject eachTranslation = resultTranslations.getJSONObject(0);
            translation = eachTranslation.getString("translatedText");
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Problem parsing the JSON results", e);
        }
        return translation;
    }
    //  PUBLIC METHOD TO FETCH TRANSLATION
    public static String fetchTranslation(String requestUrl) {
        URL url = createUrl(requestUrl);
        String jsonResponse = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem making the HTTP request.", e);

        }
        return extractFromJsonTranslation(jsonResponse);
//        return jsonResponse.toString();
    }

    //  PUBLIC METHOD TO FETCH LANGUAGES
    public static ArrayList<String> fetchLanguages(String requestUrl) {
        URL url = createUrl(requestUrl);
        String jsonResponse = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem making the HTTP request.", e);
        }
        return extractFromJsonLanguages(jsonResponse);
    }

    //  METHOD TO EXTRACT LANGUAGE LIST FROM JSON RESPONSE
    private static ArrayList<String> extractFromJsonLanguages(String stringJSON) {
        ArrayList<String> languagesList = new ArrayList<>();
        if (TextUtils.isEmpty(stringJSON)) {
            return null;
        }
        // Try to parse the JSON response string.
        try{
            JSONObject baseJsonResponse = new JSONObject(stringJSON);
            JSONObject baseJsonResponseLangs = baseJsonResponse.optJSONObject("data");
            JSONArray JsonResponselanguagesArray = baseJsonResponseLangs.optJSONArray("languages");
            LANGUAGE_CODES.clear();
            if(JsonResponselanguagesArray!=null && JsonResponselanguagesArray.length()>0){
                for (int i = 0; i < JsonResponselanguagesArray.length(); i++) {
                    JSONObject childJsonObject = JsonResponselanguagesArray.optJSONObject(i);
                    String tempLanguageCode = childJsonObject.getString("language") ;

                    switch (tempLanguageCode) {
                        case "ar":
                            LANGUAGE_CODES.add(tempLanguageCode);
                            languagesList.add(childJsonObject.getString("name"));
                            break;
                        case "bn":
                            LANGUAGE_CODES.add(tempLanguageCode);
                            languagesList.add("বাংলা");
                            break;
                        case "en":
                            LANGUAGE_CODES.add(tempLanguageCode);
                            languagesList.add(childJsonObject.getString("name"));
                            break;
                        case "gu":
                            LANGUAGE_CODES.add(tempLanguageCode);
                            languagesList.add("ગુજરાતી");
                            break;
                        case "hi":
                            LANGUAGE_CODES.add(tempLanguageCode);
                            languagesList.add("हिन्दी");
                            break;
                        case "kn":
                            LANGUAGE_CODES.add(tempLanguageCode);
                            languagesList.add("ಕನ್ನಡ");
                            break;
                        case "ml":
                            LANGUAGE_CODES.add(tempLanguageCode);
                            languagesList.add("മലയാളം");
                            break;
                        case "mr":
                            LANGUAGE_CODES.add(tempLanguageCode);
                            languagesList.add("मराठी");
                            break;
                        case "ne":
                            LANGUAGE_CODES.add(tempLanguageCode);
                            languagesList.add("नेपाली");
                            break;
                        case "pa":
                            LANGUAGE_CODES.add(tempLanguageCode);
                            languagesList.add("ਪੰਜਾਬੀ");
                            break;
                        case "ta":
                            LANGUAGE_CODES.add(tempLanguageCode);
                            languagesList.add("தமிழ்");
                            break;
                        case "te":
                            LANGUAGE_CODES.add(tempLanguageCode);
                            languagesList.add("తెలుగు");
                            break;
                        case "ur":
                            LANGUAGE_CODES.add(tempLanguageCode);
                            languagesList.add("اردو");
                            break;
                    }
                }
            }
        } catch (JSONException e) {
            Log.e("QueryUtils", "Problem parsing the JSON results", e);
        }
        return languagesList;

    }


}
