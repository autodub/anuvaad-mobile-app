package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageProxy;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.text.Text;
import com.google.mlkit.vision.text.TextRecognition;
import com.google.mlkit.vision.text.TextRecognizer;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;

import static com.example.myapplication.ConversationActivity.LOG_TAG;
import static com.example.myapplication.GlobalVars.BASE_REQ_URL;
import static com.example.myapplication.GlobalVars.DEFAULT_FROM_LANG_POS;
import static com.example.myapplication.GlobalVars.DEFAULT_LANG_POS;
import static com.example.myapplication.GlobalVars.LANGUAGE_CODES;

public class CameraTranslateActivity extends AppCompatActivity {

    private Button captureImageBtn, detectTextBtn;
    private ImageView imageView;
    private TextView textView;
    private TextView sourceTextView;
    static final int REQUEST_IMAGE_CAPTURE = 1;

    private static final String API_KEY = "Enter your api here";
    String uri = "https://translation.googleapis.com/language/translate/v2";
    String getLanguagesURI = "https://translation.googleapis.com/language/translate/v2/languages";

    private Spinner mSpinnerLanguageFrom;                                   //    Dropdown list for selecting base language (From)
    private Spinner mSpinnerLanguageTo;
    private String mLanguageCodeFrom = "en";                                //    Language Code (From)
    private String mLanguageCodeTo = "hi";
    volatile boolean activityRunning = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_translate);

        captureImageBtn = findViewById(R.id.capture_image_btn);
        imageView = findViewById(R.id.image_view);
        textView = findViewById(R.id.text_display);
        sourceTextView = findViewById(R.id.source_text_display);
        mSpinnerLanguageFrom = (Spinner) findViewById(R.id.spinner_language_from);
        mSpinnerLanguageTo = (Spinner) findViewById(R.id.spinner_language_to);



        captureImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dispatchTakePictureIntent();
            }
        });

        if (!isOnline()) {
//            mEmptyTextView.setVisibility(View.VISIBLE);
        } else {
            //  GET LANGUAGES LIST
            new GetLanguages().execute();
        }
        //  SPINNER LANGUAGE FROM
        mSpinnerLanguageFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mLanguageCodeFrom = LANGUAGE_CODES.get(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getApplicationContext(), "No option selected", Toast.LENGTH_SHORT).show();
            }
        });
        //  SPINNER LANGUAGE TO
        mSpinnerLanguageTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mLanguageCodeTo = LANGUAGE_CODES.get(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getApplicationContext(), "No option selected", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imageView.setImageBitmap(imageBitmap);
            InputImage image = InputImage.fromBitmap(imageBitmap, 0);
            TextRecognizer recognizer = TextRecognition.getClient();
            recognizer.process(image)
                    .addOnSuccessListener(
                            new OnSuccessListener<Text>() {
                                @Override
                                public void onSuccess(Text texts) {
//                                     mTextButton.setEnabled(true);
                                    processTextRecognitionResult(texts);
                                }
                            })
                    .addOnFailureListener(
                            new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    // Task failed with an exception
//                                     mTextButton.setEnableed(true);
                                    e.printStackTrace();
                                }
                            });
        }
    }

    private void processTextRecognitionResult(Text texts) {
        String tee = texts.getText(); ;
        List<Text.TextBlock> blocks = texts.getTextBlocks();
        if (blocks.size() == 0) {
            Context context = getApplicationContext();
            CharSequence text = "No text detected";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
            return;
        }
        for (int i = 0; i < blocks.size(); i++) {
            List<Text.Line> lines = blocks.get(i).getLines();
            for (int j = 0; j < lines.size(); j++) {
                List<Text.Element> elements = lines.get(j).getElements();
                for (int k = 0; k < elements.size(); k++) {
//                     Graphic textGraphic = new TextGraphic(mGraphicOverlay, elements.get(k));
//                     mGraphicOverlay.add(textGraphic);
//                     String lineText = elements.get(k);
                    sourceTextView.setText(tee);
                }
            }
        }
        sourceTextView.setText(tee);
        new TranslateText().execute(tee);

    }

    //  CHECK INTERNET CONNECTION
    public  boolean isOnline()
    {   try {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    } catch (Exception e) {
        Log.e(LOG_TAG, e.getMessage());
    }
        return false;
    }

    private class TranslateText extends AsyncTask<String,Void,String> {
        @Override
        protected String doInBackground(String... input) {
            if (input[0].isEmpty()) {
                return "";
            } else {
                Uri baseUri = Uri.parse(uri);
                Uri.Builder uriBuilder = baseUri.buildUpon();
                uriBuilder
                        .appendQueryParameter("target","hi")
                        .appendQueryParameter("key", API_KEY)
                        .appendQueryParameter("q", input[0]);
                Log.e("String Url ---->", uriBuilder.toString());
                textView.setText("Wait a Moment");
                System.out.println(uriBuilder.toString());
                textView.setText(CameraQueryUtils.fetchTranslation(uriBuilder.toString()));
                return CameraQueryUtils.fetchTranslation(uriBuilder.toString());
            }
        }
    }
    //  SUBCLASS TO GET LIST OF LANGUAGES ON BACKGROUND THREAD
    private class GetLanguages extends AsyncTask<Void,Void, ArrayList<String>> {
        @Override
        protected ArrayList<String> doInBackground(Void... params) {
            Uri baseUri = Uri.parse(getLanguagesURI);
            Uri.Builder uriBuilder = baseUri.buildUpon();
            uriBuilder
                    .appendQueryParameter("target","en")
                    .appendQueryParameter("model", "nmt")
                    .appendQueryParameter("key", API_KEY);
            Log.e("String Url ---->",uriBuilder.toString());
            return CameraQueryUtils.fetchLanguages(uriBuilder.toString());
        }
        @Override
        protected void onPostExecute(ArrayList<String> result) {
            if (activityRunning) {
                ArrayAdapter<String> adapter = new ArrayAdapter<>(CameraTranslateActivity.this, android.R.layout.simple_spinner_item, result);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                //set languages list for from (only english)
                ArrayList<String> fromLanguagesList = new ArrayList<>();
                fromLanguagesList.add("English");
                ArrayAdapter<String> adapterFrom = new ArrayAdapter<>(CameraTranslateActivity.this, android.R.layout.simple_spinner_item, fromLanguagesList);
                adapterFrom.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


                mSpinnerLanguageFrom.setAdapter(adapterFrom);
                mSpinnerLanguageTo.setAdapter(adapter);
                //  SET DEFAULT LANGUAGE SELECTIONS
                mSpinnerLanguageFrom.setSelection(0);
                mSpinnerLanguageTo.setSelection(DEFAULT_LANG_POS);
            }
        }
    }
}