package com.example.myapplication;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

public class HomeActivity extends AppCompatActivity {
    RadioGroup radioGroup1;
    RadioGroup radioGroup2;
    RadioGroup radioGroup3;
    RadioButton radioButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ConstraintLayout layout = (ConstraintLayout) findViewById(R.id.home_container);
        AlphaAnimation animation = new AlphaAnimation(0.0f , 1.0f ) ;
        animation.setFillAfter(true);
        animation.setDuration(1200);
        layout.startAnimation(animation);
        Button bConversation = (Button) findViewById(R.id.start_new_conversation);
        Button bCameraTranslation = (Button) findViewById(R.id.camera_translation);
        Button bVideoTranslation = (Button) findViewById(R.id.button_video_translate);
        radioGroup1 = findViewById(R.id.radio_group_languages_1);
        radioGroup2 = findViewById(R.id.radio_group_languages_2);
        radioGroup3 = findViewById(R.id.radio_group_languages_3);

        bConversation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, ConversationActivity.class);
                startActivity(intent);
            }
        });
        bCameraTranslation.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, CameraTranslateActivity.class);
                startActivity(intent);
            }
        });
        bVideoTranslation.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
    }

    public void showDialog() {
        Dialog dialog = new Dialog(HomeActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_design);
        dialog.show();
    }

    public void onRadioButtonClicked1(View view) {
        Button bConversation = (Button) findViewById(R.id.start_new_conversation);
        Button bCameraTranslation = (Button) findViewById(R.id.camera_translation);
        Button bVideoTranslation = (Button) findViewById(R.id.button_video_translate);
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        radioGroup2.clearCheck();
        radioGroup3.clearCheck();
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_english:
                if (checked){
                    bConversation.setText("  Speech Translate  ");
                    bCameraTranslation.setText("  Photo Translate  ");
                    bVideoTranslation.setText("  Audio / Video Translate  ");}
                    break;
            case R.id.radio_hindi:
                if (checked){
                    bConversation.setText("  भाषण अनुवाद  ");
                    bCameraTranslation.setText("  फोटो अनुवाद  ");
                    bVideoTranslation.setText("  ऑडियो / वीडियो अनुवाद  ");}
                    break;
            case R.id.radio_bengali:
                if (checked){
                    bConversation.setText("  ্পিচ অনুবাদ  ");
                    bCameraTranslation.setText("  ফটো অনুবাদ  ");
                    bVideoTranslation.setText("  অডিও / ভিডিও অনুবাদ  ");}
                break;
        }
    }
    public void onRadioButtonClicked2(View view) {
        Button bConversation = (Button) findViewById(R.id.start_new_conversation);
        Button bCameraTranslation = (Button) findViewById(R.id.camera_translation);
        Button bVideoTranslation = (Button) findViewById(R.id.button_video_translate);
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        radioGroup1.clearCheck();
        radioGroup3.clearCheck();
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_gujrathi:
                if (checked){
                    bConversation.setText("  ભાષણ અનુવાદ  ");
                    bCameraTranslation.setText("  ફોટો અનુવાદ  ");
                    bVideoTranslation.setText("  ઓડિયો / વિડિઓ અનુવાદ  ");}
                break;
            case R.id.radio_telugu:
                if (checked){
                    bConversation.setText("  ప్రసంగ అనువాదం  ");
                    bCameraTranslation.setText("  ఫోటో అనువాదం  ");
                    bVideoTranslation.setText("  ఆడియో / వీడియో అనువాదం  ");}
                break;
            case R.id.radio_tamil:
                if (checked){
                    bConversation.setText("  பேச்சு மொழிபெயர்ப்பு ");
                    bCameraTranslation.setText("  புகைப்பட மொழிபெயர்ப்பு  ");
                    bVideoTranslation.setText("  ஆடியோ / வீடியோ மொழிபெயர்ப்பு  ");}
                break;
        }
    }
    public void onRadioButtonClicked3(View view) {
        Button bConversation = (Button) findViewById(R.id.start_new_conversation);
        Button bCameraTranslation = (Button) findViewById(R.id.camera_translation);
        Button bVideoTranslation = (Button) findViewById(R.id.button_video_translate);
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        radioGroup1.clearCheck();
        radioGroup2.clearCheck();
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_kannada:
                if (checked){
                    bConversation.setText("  ಭಾಷಣ ಅನುವಾದ  ");
                    bCameraTranslation.setText("  ಫೋಟೋ ಅನುವಾದ  ");
                    bVideoTranslation.setText("  ಆಡಿಯೋ / ವಿಡಿಯೋ ಅನುವಾದ  ");}
                break;
            case R.id.radio_malyalam:
                if (checked){
                    bConversation.setText("  സംഭാഷണ വിവർത്തനം  ");
                    bCameraTranslation.setText("  ഫോട്ടോ വിവർത്തനം  ");
                    bVideoTranslation.setText("  ഓഡിയോ / വീഡിയോ വിവർത്തനം  ");}
                break;
            case R.id.radio_marathi:
                if (checked){
                    bConversation.setText("  भाषण भाषांतर  ");
                    bCameraTranslation.setText("  फोटो भाषांतर  ");
                    bVideoTranslation.setText("  ऑडिओ / व्हिडिओ भाषांतर  ");}
                break;
        }
    }

}
